---
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
type: reference
---

> Introduced in GitLab 15.11 [with a flag](../../../administration/feature_flags.md) named `remote_development_feature_flag`. Disabled by default.

FLAG:
On self-managed GitLab, by default this feature is not available. To make it available,
ask an administrator to [enable the feature flag](../../../administration/feature_flags.md) named `remote_development_feature_flag`.
On GitLab.com, this feature is not available.
The feature is not ready for production use.

# Tutorial: Create and run your first GitLab Workspace **(ULTIMATE)**

This tutorial shows you how to configure and run your first Remote Development Workspace in GitLab.
