---
stage: Create
group: Editor
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Web IDE Beta **(FREE)**

> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/95169) in GitLab 15.7 [with a flag](../../../administration/feature_flags.md) named `vscode_web_ide`. Disabled by default.
> - [Enabled on GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/issues/371084) in GitLab 15.7.
> - [Enabled on self-managed](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115741) in GitLab 15.11.

FLAG:
On self-managed GitLab, by default this feature is available. To hide the feature, ask an administrator to [disable the feature flag](../../../administration/feature_flags.md) named `vscode_web_ide`. On GitLab.com, this feature is available.

As announced in [this blog post](https://about.gitlab.com/blog/2022/05/23/the-future-of-the-gitlab-web-ide/),
the current implementation of the [Web IDE](../web_ide/index.md) is being replaced
with an implementation powered by Visual Studio Code. This effort is still under
development. For updates, see [this epic](https://gitlab.com/groups/gitlab-org/-/epics/7683).

To pair the Web IDE Beta with a Remote Development environment, see [Remote Development](../remote_development/index.md).

## Use the Web IDE Beta

To open the Web IDE Beta from anywhere in the UI:

- Use the <kbd>.</kbd> [keyboard shortcut](../../shortcuts.md).

You can also open the Web IDE Beta when viewing a file, the repository file list,
or a merge request.

### Use when viewing a file or the repository file list

To open the Web IDE Beta from a file or the repository file list:

- In the upper-right corner of the page, select **Open in Web IDE**.

If **Open in Web IDE** is not visible:

1. Next to **Edit** or **Gitpod**, select the down arrow (**{chevron-lg-down}**).
1. From the list, select **Open in Web IDE**.
1. Select **Open in Web IDE**.

### Use when viewing a merge request

To open the Web IDE Beta from a merge request:

1. Go to your merge request.
1. In the upper-right corner, select **Code > Open in Web IDE**.

The Web IDE Beta opens new and modified files in separate tabs and displays changes side by side with the original source. To optimize loading time, only the top 10 files (by number of lines changed) are opened automatically.

In the file tree, any new or modified file in the merge request is indicated by an icon next to the filename. To view changes to a file, right-click the filename and select **Compare with merge request base**.

## Open a file in the Web IDE Beta

To open any file by its name:

1. Press <kbd>Command</kbd>+<kbd>P</kbd>.
1. Enter the name of your file.

![fuzzy_finder_v15_7](img/fuzzy_finder_v15_7.png)

## Switch branches

The Web IDE Beta uses the currently selected branch by default.
To switch branches in the Web IDE Beta:

1. On the status bar, in the lower-left corner, select the current branch name.
1. In the search box, start typing the branch name.
1. From the dropdown list, select the branch.

## Create a branch

To create a branch from the current branch in the Web IDE Beta:

1. On the status bar, in the lower-left corner, select the current branch name.
1. From the dropdown list, select **Create new branch...**.
1. Enter the branch name.
1. Press <kbd>Enter</kbd>.

If you don't have write access to the repository, **Create new branch...** is not visible.

## Search across files

You can use VS Code to quickly search all files in the opened folder.

To search across files:

1. Press <kbd>Shift</kbd>+<kbd>Command</kbd>+<kbd>F</kbd>.
1. Enter your search term.

In the Web IDE Beta, only partial results from opened files are displayed.
Full file search is planned for a later date.

## View a list of changed files

To view a list of files you changed in the Web IDE Beta,
on the Activity Bar on the left, select **Source Control**.
Your `CHANGES`, `STAGED CHANGES`, and `MERGE CHANGES` are displayed.

For details, see the [VS Code documentation](https://code.visualstudio.com/docs/sourcecontrol/overview#_commit).

## Commit changes

To commit your changes in the Web IDE Beta:

1. On the Activity Bar on the left, select **Source Control**,
or press <kbd>Control</kbd>+<kbd>Shift</kbd>+<kbd>G</kbd>.
1. Enter your commit message.
1. Select **Commit & Push**.
1. Commit to the current branch, or create a new branch.

## Open the command palette

In the Web IDE Beta, you can access many commands through the command palette.
To open the command palette and run a command in the Web IDE Beta:

1. Press <kbd>F1</kbd> or <kbd>Shift</kbd>+<kbd>Command</kbd>+<kbd>P</kbd>.
1. In the search box, start typing the command name.
1. From the dropdown list, select the command.

## Stop using the Web IDE Beta

If you do not want to use the Web IDE Beta, you can change your personal preferences.

1. On the top bar, in the upper-right corner, select your avatar.
1. Select **Preferences**.
1. In the **Web IDE** section, select the **Opt out of the Web IDE Beta** checkbox.
1. Select **Save changes**.

## Known issues

The [Web Terminal](../web_ide/index.md#interactive-web-terminals-for-the-web-ide)
and [Live Preview](../web_ide/index.md#live-preview-removed) are not available in the Web IDE Beta.

These features might become available at a later date.
